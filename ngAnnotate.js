#!/usr/bin/env node --harmony

/*
* @Author: Leander Dirkse
* @Date:   2016-04-28 13:34:49
* @Last Modified by:   leander
*/

'use strict';

const fs         = require('fs');
const glob       = require('glob');
const ngAnnotate = require('ng-annotate');

let srcFolder  = process.argv.slice(2)[0];

/**
 * Find all files that match a glob pattern in given directories
 * @param  {array} path     Which directories to look in
 * @return {array}          An array of all the files that were found
 */
const findFiles       = path => [].concat( glob.sync( `${path}/**/*.js` ) );

/**
 * Get the contents of a file
 * @param  {string} file    The path to the file you want to read
 * @return {string}         The contents of the file
 */
const getFileContents = file => fs.readFileSync( file, 'utf8' );

const getFilename     = file => file.split( '/' ).pop();

const annotate = files => { 
    for( let file of files ) {
        
        let src = getFileContents( file );
       
        let result = ngAnnotate(src, {
            add: true,
            single_quotes : true
        });

        console.log( `Writing file ${ getFilename( file ) }` );
        fs.writeFileSync( file, result.src );
    }

}

annotate( findFiles( srcFolder ) );