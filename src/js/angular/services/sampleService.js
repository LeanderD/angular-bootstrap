/*
* @Author: Leander Dirkse
* @Date:   2016-05-26 20:05:01
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:06:07
*/

'use strict';

angular
    .module(App.Config.computedName)
    .service('SampleService', function _sampleService ( $http, config, $q ) {
        var SampleService = {};

        SampleService.something = function _getSomething( options ) {
            var options       = options || {};
            var defaultParams = { type : 'iets' };

            var params = $.extend( {}, defaultParams, options );

            return $http.get( config.apiUrl + '/something', { params: params } );
        }

        return SampleService;
    });
