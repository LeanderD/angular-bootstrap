/*
* @Author: Leander Dirkse
* @Date:   2016-05-26 20:01:59
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:04:36
*/

'use strict';

angular.module(App.Config.computedName)
    .factory('User', function _userFactory( $http, $q, config ) {
        return function _sample( data ) {
            data = data || {};

            return {
                _id         : data._id                          || ''
                , username  : data.username                     || ''
                , password  : data.password                     || ''
                , login     : function _login() {

                    var deferred = $q.defer();
                    var data     = { username: this.username, password: this.password };
                    var user     = this;

                    $http.post( config.apiUrl + '/login', data )
                        .then( function _fulfilled( response ) {
                            user._id = response.data._id;

                            deferred.resolve( user.toObject() );
                        })
                        .then( null, function _rejected( response ) {
                            deferred.reject( response );
                        });

                    return deferred.promise;
                }
                , toObject  : function _toObject() {
                    var object = angular.copy( this );

                    delete object.login;
                    delete object.password;
                    delete object.toObject;

                    return object;
                }
            }
        }
    });