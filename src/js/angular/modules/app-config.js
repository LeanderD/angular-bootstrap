/*
* @Author: Leander Dirkse
* @Date:   2016-04-15 23:31:43
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:35:18
*/

'use strict';

angular
    .module( App.Config.computedName )
    .config( function _ngModuleConfig( $stateProvider, $urlRouterProvider, $compileProvider ) {

        // LD: only for production
        /*$compileProvider.debugInfoEnabled( false );*/

        // LD: For any unmatched url, send to /
        $urlRouterProvider.otherwise( '/home' );

        // LD: application states
        $stateProvider
            .state( 'app', {
                abstract      : true
                , url         : '/'
                , templateUrl : 'views/app.html'
            })
                .state( 'app.home', {
                    url     : 'home'
                    , templateUrl : 'views/app.home.html'
                    , views : {
                        'view__app' : {
                            templateUrl  : 'views/app.home.html'
                            , controller : 'SampleController'
                        }
                    }
                })
                .state( 'app.about', {
                    url     : 'about'
                    , views : {
                        'view__app' : {
                            templateUrl : 'views/app.about.html'
                        }
                    }
                })
    })
    .value( 'config', App.Config );