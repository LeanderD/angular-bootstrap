/*
* @Author: Leander Dirkse
* @Date:   2016-04-15 23:30:17
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:50:41
*/

'use strict';

angular
    .module( App.Config.computedName )
    .run( function _ngModuleRun ( $rootScope, $state, $stateParams ) {

        // LD: remove for production
        $rootScope.$on( '$stateChangeError', console.log.bind( console ) );

        $rootScope.stateParams = $stateParams;

        $rootScope.$on( '$stateChangeSuccess', function _stateChangeSuccess() {
            $rootScope.state = $state.current;
        });
    });