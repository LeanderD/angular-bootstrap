/*
* @Author: Leander Dirkse
* @Date:   2016-04-15 23:21:52
* @Last Modified by:   leander
* @Last Modified time: 2016-04-15 23:22:17
*/

'use strict';

angular
    .module( App.Config.computedName, App.Config.dependencies )
    .constant( 'AppConfig', App.Config )