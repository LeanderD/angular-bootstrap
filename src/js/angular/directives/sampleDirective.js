/*
* @Author: Leander Dirkse
* @Date:   2016-05-10 21:26:46
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:36:11
*/

'use strict';

angular
    .module( App.Config.computedName )
    .directive( 'sample', function() {
        return {
            replace       : true
            , restrict    : 'E'
            , templateUrl : 'components/sample.html'
        }
    });