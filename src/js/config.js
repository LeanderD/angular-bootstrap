/*
* @Author: Leander Dirkse
* @Date:   2016-05-03 21:10:48
* @Last Modified by:   leander
* @Last Modified time: 2016-05-26 20:51:13
*/

'use strict';

var App = ( function _appConfig( App, window, undefined ) {

    App.Config = {
        name            : 'AngularBootstrap'
        , computedName  : 'AngularBootstrap'
        , dependencies  :  [
            'ui.router'
            , 'ngAnimate'
        ]
        , apiUrl       : window.location.host === 'localhost:3000' ?  'http://api.lokaal' : 'http://api.niet-lokaal.nl'
    };

    return App;

}( App || {}, window ) );